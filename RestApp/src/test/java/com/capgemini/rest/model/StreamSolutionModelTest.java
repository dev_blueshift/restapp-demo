package com.capgemini.rest.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * @author Edwin Cheah
 * 
 * Test business logic in StreamSolutionModel class
 */
class StreamSolutionModelTest {	
	private static StreamSolutionModel solution;
	private static ArrayList<Integer> testData;
	
	@BeforeAll
	static void setup() {
		//Initialize test subject
		solution = new StreamSolutionModel();
		
		//Prepare test data
		testData = new ArrayList<Integer>();
		testData.add(5);
		testData.add(4);
		testData.add(6);
		testData.add(1);
	}

	/*
	 * Ensures full parsing flow is successful which includes 
	 * a. Calculation
	 * b. Creation of response payload (JSONObject)
	 */
	@Test
	void testParseData() {			
		Object result = solution.parseData(testData);		
	    assertTrue(result instanceof JSONObject);
	}
	
	/*
	 * Ensures calculation is accurate
	 */
	@Test
	void testCalculate() {
		String result = solution.calculate(testData);
		assertEquals("8.77", result);
	}

	/*
	 * Ensures response JSONObject can be created to hold calculated result
	 */
	@Test
	void testCreateResponsePayload() {
		//Simulates data that comes from completing calculate() method call.
		String testData = "8.77";

		//Check object type
		Object result = solution.createResponsePayload(testData);
		assertTrue(result instanceof JSONObject);

		//Check content in response payload. (JSONObject)
		JSONObject jsonObject =  (JSONObject) result;
		String dataContent = (String) jsonObject.get("output");
		assertEquals("8.77", dataContent);
	}

	@AfterAll
	static void tearDown() {
		solution = null;
		testData = null;
	}	
}