package com.capgemini.rest.ctrl;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.rest.model.StreamSolutionModel;

/**
 * @author Edwin Cheah
 * 
 * Request handler for incoming HTTP requests.
 */
@RestController
public class RestApplicationController {
	@Autowired
	private ApplicationContext applicationContext;

	/**
	 * End Point: http://localhost:8080
	 * @return String Simple message to indicate application's state
	 */
	@RequestMapping("/")	
	public String heartBeat() {
		return "Application Heartbeat - Rest application up and running";
	}
	
	/**
	 * End Point: http://localhost:8080/calculate
	 * @return Object JSON object carrying computed result. 
	 */
	@PostMapping(value = "/calculate") 
	public Object calculate(@RequestBody Map<String, Object> payload) {	
		StreamSolutionModel solutionModel = applicationContext.getBean(StreamSolutionModel.class);
		
		if (payload.get("data") instanceof ArrayList) {
			ArrayList<Integer> data = (ArrayList<Integer>) payload.get("data");		
			return solutionModel.parseData(data);
		}

		return "";
	}
}