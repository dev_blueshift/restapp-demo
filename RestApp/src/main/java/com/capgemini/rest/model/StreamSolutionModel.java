package com.capgemini.rest.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author Edwin Cheah
 * 
 * Business Logic to address functional requirements.
 */
@Component
public class StreamSolutionModel implements ApplicationContextAware {
	private DecimalFormat decimalFormat;

	/**
	 * Constructor
	 */
	public StreamSolutionModel() {
		decimalFormat = new DecimalFormat("#.##");
	}
	
	/**
	 * Parse incoming JSON payload & return result.
	 * 
	 * @param payload	Incoming payload from POST request 
	 * 
	 * @return 			Outgoing payload for request response
	 */
	public Object parseData(ArrayList<Integer> payload) {
		String formattedResult = calculate(payload);
		return createResponsePayload(formattedResult);
	}

	/**
	 * Using Java Stream to solve 
	 *    a. Sort & select 3 Highest numeric input
	 *    b. Square each number
	 *    c. Sum
	 *    d. Square root
	 *  
	 * @return	Computed results after formatted to 2 decimal points
	 */
	protected String calculate(ArrayList<Integer> payload) {
		List<Integer> numList = payload;

		double doubleResult = numList.stream()								//Stream<Integer>
				 					 .sorted((e2, e1) -> e1.compareTo(e2))  //Sort descending
				 					 .limit(3)							    //Take 3 element (Highest numeric input)
				 					 .mapToDouble((v) -> Math.pow(v, 2))    //Square each element
				 					 .sum();							    //Sum each element

		return decimalFormat.format(Math.sqrt(doubleResult));				//Square root formatted to 2 decimal points.
	}

	/**
	 * Construct outgoing JSON object where "output" is assigned to calculated responsePayload.
	 * 
	 * Note:
     *    Simple JSON dependency was added from "com.googlecode.json-simple" refer pom.xml dependency.
	 * 
	 *  @param responsePayload	Outgoing response payload
	 */
	protected Object createResponsePayload(String responsePayload) {
		JSONObject result = new JSONObject();
		result.put("output", responsePayload);
		return result;
	}
	
	/**
	 * Implemented interface ApplicationContextAware to enabling loading of this class via IoC
	 * Method not use for initialization of this class. Constructor was sufficient. 
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {}
}