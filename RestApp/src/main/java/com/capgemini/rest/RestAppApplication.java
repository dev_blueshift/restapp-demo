package com.capgemini.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Edwin Cheah
 * 
 * Application's starting point. 
 */
@SpringBootApplication
public class RestAppApplication {
	public static void main(String[] args) {
		SpringApplication.run(RestAppApplication.class, args);
	}
}