# README #
### How do I get set up? ###
This README documents steps for 2 objectives:  
&nbsp;&nbsp;&nbsp;&nbsp; 1. For quickly starting an applcation - Start REST application & test via Postman.  
&nbsp;&nbsp;&nbsp;&nbsp; 2. Code review - Import project into IDE for source review & run applciation from IDE.  

Content purpose in **restapp-demo** Git repository:  
&nbsp;&nbsp;&nbsp;&nbsp; RestApp     - Projet folder, contains application source code  
&nbsp;&nbsp;&nbsp;&nbsp; RestApp_lib - (Project dependencies)  
&nbsp;&nbsp;&nbsp;&nbsp; RestApp.jar - (Executable JAR)  
&nbsp;&nbsp;&nbsp;&nbsp; RestApp-Start.bat - Convenient start up batch file to run Executable JAR  
&nbsp;&nbsp;&nbsp;&nbsp; RestApp-Capgemini.postman_collection.json - Contains configuraton to simulate HTTP request calls via Postmen application.  

**Note:** Project Files & README.md viewable via **https://dev_blueshift@bitbucket.org/dev_blueshift/restapp-demo.git** (Refresh page Bitbucket loads slow)  

1) Simply run & play with application - Start REST application & test via Postman.  
&nbsp;&nbsp;&nbsp;&nbsp; Step 1: git clone https://dev_blueshift@bitbucket.org/dev_blueshift/restapp-demo.git  
&nbsp;&nbsp;&nbsp;&nbsp; Step 2: cd restapp-demo  
&nbsp;&nbsp;&nbsp;&nbsp; Step 3: double click on **RestApp-Start.bat** to start SprintBoot application  
&nbsp;&nbsp;&nbsp;&nbsp; Step 4: Test function with following end points:  
&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; For Get request : http://localhost:8080 (via Browser)  
&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; For Post request: http://localhost:8080/calculate (via Postman)  

&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; **Note:**  
&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; **RestApp-Capgemini.postman_collection.json** file is provided if one wishes to test 2nd end points via Postman application.  
&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Simply import this file into Postman application:

&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; On Postman application --> File --> Import --> Upload / drop **RestApp-Capgemini.postman_collection.json** into Postman --> Click send on the follwoing end point configs.  
&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; a. GET Hello  
&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; b. POST Calculate (JSON object is available in RestApp-Capgemini.postman_collection.json config file for testing via Postman)

2) Code review - Import project into IDE for source review & run applciation from IDE.  
&nbsp;&nbsp;&nbsp;&nbsp; On IDE (Eclipse 2021.12 / Spring Tool Suite 4) --> Import --> Existing project into workspace --> Select Root Directory (RestApp folder) --> Finish  
&nbsp;&nbsp;&nbsp;&nbsp; Application & Unit testing can be run via IDE. Postman if setup, can be used to push HTTP requests to SpringBoot application.  
